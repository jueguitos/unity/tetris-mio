﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaTetromino : MonoBehaviour
{
    private float tiempoAnterior;
    public float tiempoCaida = .8f;

    public static int alto = 21;
    public static int ancho = 11;

    public Vector3 puntoRotacion;

    public static int puntaje = 0;
    public static int nivelDeDificultad = 0;

    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //Desplazamiento vertical piezas
        if (Time.time - tiempoAnterior > (Input.GetKey(KeyCode.DownArrow) ? tiempoCaida / 20 : tiempoCaida))
        {
            transform.position += new Vector3(0, -1, 0);
            if (!FindObjectOfType<LogicaGenerador>().Limites(transform))
            {
                transform.position -= new Vector3(0, -1, 0);

                FindObjectOfType<LogicaGenerador>().AñadirAlGrid(transform);
                FindObjectOfType<LogicaGenerador>().RevisarLineas();

                this.enabled = false;
                FindObjectOfType<LogicaGenerador>().NuevoTetromino();
            }

            tiempoAnterior = Time.time;
        }


        //Desplazamiento horizontal piezas
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            transform.position += new Vector3(-1, 0, 0);
            if (!FindObjectOfType<LogicaGenerador>().Limites(transform))
            {
                transform.position -= new Vector3(-1, 0, 0);
            }
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            transform.position += new Vector3(1, 0, 0);
            if (!FindObjectOfType<LogicaGenerador>().Limites(transform))
            {
                transform.position -= new Vector3(1, 0, 0);
            }
        }

        //Rotar piezas
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            transform.RotateAround(transform.TransformPoint(puntoRotacion), new Vector3(0, 0, 1), -90);

            if (!FindObjectOfType<LogicaGenerador>().Limites(transform))
            {
                transform.RotateAround(transform.TransformPoint(puntoRotacion), new Vector3(0, 0, 1), 90);
            }
            else
            {
                audioSource.Play();
            }
        }

    }

}
