﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LogicaGenerador : MonoBehaviour
{
    public static int alto = 21;
    public static int ancho = 11;
    public GameObject[] tetrominos;
    public int puntaje = 0;
    public static Transform[,] grid = new Transform[ancho, alto];


    [SerializeField]
    private AudioClip _audioClip;
    private AudioSource _audioSource;

    public ParticleSystem _particles;
    public ParticleSystem ps;

    // Start is called before the first frame update
    void Start()
    {
        GameObject generadorTetrominos = GameObject.Find("GeneradorTetrominos");
        _audioSource = generadorTetrominos.GetComponent<AudioSource>();
        _particles = gameObject.GetComponentInChildren<ParticleSystem>();

        if (_audioSource == null)
        {
            Debug.LogError("The AudioSource in the player NULL!");
        }
        else {
            _audioSource.clip = _audioClip;
        }
        NuevoTetromino();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public float tiempoCaida {
        get{
            switch (nivelDeDificultad) {
                case 1:
                    return .8f;

                case 2:
                    return .65f;

                case 3:
                    return .55f;

                case 4:
                    return .45f;

                case 5:
                    return .35f;

                case 6:
                    return .3f;

                case 7:
                    return .25f;

                case 8:
                    return .2f;

                case 9:
                    return .25f;

                case 10:
                    return .15f;

                default:
                    return .1f;
            }
        }
    }

    public int nivelDeDificultad
    {
        get {
            if (puntaje >= 200 && puntaje < 400)
            {
                return 2;
            }
            else if (puntaje >= 400 && puntaje < 600)
            {
                return 3;
            }
            else if (puntaje >= 600 && puntaje < 900)
            {
                return 4;
            } 
            else if (puntaje >= 900 && puntaje < 1100)
            {
                return 5;
            } 
            else if (puntaje >= 1100 && puntaje < 1500)
            {
                return 6;
            } 
            else if (puntaje >= 1500 && puntaje < 1700)
            {
                return 7;
            }
            else if (puntaje >= 1700 && puntaje < 2000)
            {
                return 8;
            }
            else if (puntaje >= 2000 && puntaje < 2200)
            {
                return 9;
            }
            else if (puntaje >= 2200)
            {
                return 10;
            } 
            else {
                return 1;
            }
        }
    }

    public void NuevoTetromino()
    {
        _audioSource.Play();

        _particles = gameObject.GetComponentInChildren<ParticleSystem>();

        if (Limites(gameObject.transform))
        {
            GameObject tetromino = Instantiate(tetrominos[Random.Range(0, tetrominos.Length)], transform.position, Quaternion.identity);
            tetromino.GetComponent<LogicaTetromino>().tiempoCaida = tiempoCaida;
        }
        else {
            ReiniciarPartida();
        }

        Debug.Log("Tiene línea");
    }

    public void ReiniciarPartida() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void AñadirAlGrid(Transform tetromino)
    {
        foreach (Transform hijo in tetromino)
        {
            int enteroX = Mathf.RoundToInt(hijo.transform.position.x);
            int enteroY = Mathf.RoundToInt(hijo.transform.position.y);

            grid[enteroX, enteroY] = hijo;

            if (enteroY >= alto - 1)
            {
                puntaje = 0;

                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }

    public void RevisarLineas()
    {
        for (int i = alto - 1; i >= 0; i--)
        {
            if (TieneLinea(i))
            {
                BorrarLinea(i);
                BajarLinea(i);
            }
        }
    }

    bool TieneLinea(int i)
    {
        for (int j = 0; j < ancho; j++)
        {
            if (grid[j, i] == null)
            {
                return false;
            }
        }

        puntaje += 100;

        DepurarValores();

        return true;
    }

    void BorrarLinea(int i)
    {
        for (int j = 0; j < ancho; j++)
        {

            ps = Instantiate(_particles, new Vector3(j, i, 0), Quaternion.identity);
            ParticleSystem.MainModule ma = ps.main;
            ma.startColor = grid[j, i].gameObject.GetComponent<SpriteRenderer>().color;

            ps.Play();
            Destroy(ps.gameObject, 1000f);

            Destroy(grid[j, i].gameObject);
            grid[j, i] = null;
        }
    }

    void BajarLinea(int i)
    {
        for (int y = i; y < alto; y++)
        {
            for (int j = 0; j < ancho; j++)
            {
                if (grid[j, y] != null)
                {
                    grid[j, y - 1] = grid[j, y];
                    grid[j, y] = null;
                    grid[j, y - 1].transform.position -= new Vector3(0, 1, 0);
                }
            }
        }
    }

    public bool Limites(Transform tetromino)
    {
        for (int i = 0; i < tetromino.childCount; i++)
        {
            var square = tetromino.GetChild(i);

            int enteroX = Mathf.RoundToInt(square.transform.position.x);
            int enteroY = Mathf.RoundToInt(square.transform.position.y);

            if (enteroX < 0 || enteroX >= ancho || enteroY < 0 || enteroY >= alto)
            {
                return false;
            }


            if (grid[enteroX, enteroY] != null)
            {
                return false;
            }
        }

        return true;
    }

    public void DepurarValores() {
        Debug.Log("Puntaje: " + puntaje + "; Nivel de dificultad: " + nivelDeDificultad.ToString() + "; Tiempo Caída: " + tiempoCaida.ToString());
    }
}
